package com.abocode.jfaster.core.common.dao.impl;


import com.abocode.jfaster.core.common.dao.IGenericBaseCommonDao;
import com.abocode.jfaster.core.common.dao.ICommonDao;
import org.springframework.stereotype.Repository;

/**
 * 公共扩展方法
 * @author  张代浩
 *
 */
@Repository
public  class CommonDao extends GenericBaseCommonDao implements ICommonDao, IGenericBaseCommonDao {

}
