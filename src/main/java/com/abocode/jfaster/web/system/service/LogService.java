/**
 * 
 */
package com.abocode.jfaster.web.system.service;

import com.abocode.jfaster.core.common.service.CommonService;


/**
 * 日志Service接口
 * @author  方文荣
 *
 */
public interface LogService extends CommonService{

}
