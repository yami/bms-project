package com.abocode.jfaster.web.system.entity;

import com.abocode.jfaster.core.common.entity.IdEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * TSRoleUser entity.
 * @author  张代浩
 */
@Entity
@Table(name = "t_s_role_user")
public class RoleUser extends IdEntity implements java.io.Serializable {
	private User TSUser;
	private Role TSRole;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userid")
	public User getTSUser() {
		return this.TSUser;
	}

	public void setTSUser(User TSUser) {
		this.TSUser = TSUser;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "roleid")
	public Role getTSRole() {
		return this.TSRole;
	}

	public void setTSRole(Role TSRole) {
		this.TSRole = TSRole;
	}

}