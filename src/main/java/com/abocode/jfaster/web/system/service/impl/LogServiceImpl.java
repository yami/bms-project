package com.abocode.jfaster.web.system.service.impl;

import com.abocode.jfaster.core.common.service.impl.CommonServiceImpl;
import com.abocode.jfaster.web.system.service.LogService;
import org.springframework.stereotype.Service;

/**
 * 日志Service接口实现类
 * @author  方文荣
 *
 */
@Service("logService")
public class LogServiceImpl extends CommonServiceImpl implements LogService{

}
