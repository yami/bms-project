package com.abocode.jfaster.web.system.service;

import com.abocode.jfaster.core.common.service.CommonService;

public interface TemplateService extends CommonService{
    void setDefault(String id);
}
